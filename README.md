# Cinema App Challenge

Lo scopo di questa challenge è di creare una applicazione fullstack per una sala cinematografica

<b>Requisiti:</b>

<ul>
 <li>Creare <b>schema logico</b> per descrivere la soluzione</li>
 <li>Creare <b>frontEnd</b>:
  <ul>
   <li>Creare una lista dei film in programmazione</li>
   <li>i film hanno una durata in sala da 1 a 3 settimane</li>
   <li>Creare uno storico dove i gestori della piattavorma possono accedere</li>
   <li>Possibilità di filtrare l'elenco dei film
</li>
   <li>Optional
    <ul>
     <li>Realizzare un web server rest in spring boot</li>
    </ul>
   </li>
  </ul>
 </li>
 
<li><b>Rilascio</b></b>:
 <ul>
   <li>Repository per il codice sorgente (puoi caricare il lavoro su GitHub o simili e inviarci il link; non sono accettati ZIP o TAR)</li>
   <li>Una breve presentazione del lavoro svolto (PowerPoint, Keynote, PDF, Web)</li>
   <li>Altri documenti a tua discrezione (diagrammi UML, documentazione, etc...)</li>
  </ul>
 </li>
</ul>

## Architettura

<ul>
 <li>Struttura del progetto
  <ul>
   <li>client - Single page application con Angular 17</li>
   <li>server - CRUD RESTFul APIs con Java Spring Boot 3</li>
  </ul>
 </li>
</ul>

## Output

Nel database sono stati caricati 10 film per testare l'applicativo, nel caso si vogliano aggiungere bisogna inserirli manualmente tramite uno script.

#### App cinema Home

<img src="/src/assets/screenshot/home.png" width="850">

## Tools

Per l'applicazione serve:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)
- [Node 19](https://nodejs.org/en/)
- MySql
- InteliJ, Visual Studio Code

## Technology Stack

### Server

- Tomcat server
- Spring Boot
- Spring Web
- Spring JPA
- Spring Security
- Lombok

### Client

- Angular 17.2
- Bootstrap 5
- Agular material

### Development

#### Avviare backend server

Il server è in ascolto sulla porta 8484

#### Start the client

1. Andare nella cartella del progetto e installare le dipendenze:

```bash
npm install
```

2. Lanciare il server in modalità dev e aprire `localhost:4200` nel browser:

```bash
npm start
```
3. Lanciare il docker compose per avviare il db alla porta 3307:

```bash
docker-compose up
```

4. per entrare nello storico ed eseguire il login bisogna creare un utente tramite l'api /sigup dopo aver avviato il server
bisogna passare nel body fullName,email,password


## Struttura del progetto

```
src/                         project source code
|- app/                      app components
|  |- apps/                  features application
|  |  |-admin/               module admin controlled by guard
|  |  |-film/                module film home
|  |- core/                  core module (singleton services and single-use components)
|  |- shared/                shared module  (common components, directives and pipes)
|  |- app.component.*        app root component (shell)
|  |- app.config.ts          app configuration bootstart  
|  |- app.routing.ts         app routes
|- assets/                   app assets (images, fonts, sounds...)
|- environments/             values for various build environments
|- index.html                index page
|- styles.css                global style entry point
|- main.ts                   app entry point
```



