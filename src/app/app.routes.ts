import { Routes } from '@angular/router';
import { FilmComponent } from './apps/film/film.component';
import { FilmDetailsComponent } from './apps/film/components/film-details/film-details.component';
import { LoginComponent } from './core/layout/login/login.component';

export const routes: Routes = [
  {path: '', component:FilmComponent},
  {path: 'film', component: FilmComponent},
  {path: 'film/:id', component: FilmDetailsComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'admin',
    loadChildren: () => import('./apps/admin/admin.routes')
    .then(r => r.ADMIN_ROUTES)
  }
];
