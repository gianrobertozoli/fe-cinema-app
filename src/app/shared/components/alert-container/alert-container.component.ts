import { Component, Input } from '@angular/core';
import { Alert } from '../../../core/models/alert';
import { AlertComponent } from '../alert/alert.component';

@Component({
  selector: 'app-alert-container',
  standalone: true,
  imports: [AlertComponent],
  templateUrl: './alert-container.component.html',
  styleUrl: './alert-container.component.css'
})
export class AlertContainerComponent {
  @Input() alert: Alert | undefined;
}
