import { Component, Input } from '@angular/core';
import { Alert } from '../../../core/models/alert';
import { take, timer } from 'rxjs';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-alert',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './alert.component.html',
  styleUrl: './alert.component.css'
})
export class AlertComponent {
  @Input() alert: Partial<Alert> | undefined;
  @Input() dismissible = true;

  constructor() { }

  ngOnInit() {
    if (this.alert?.dismissAfter) {
      this.dismissAlertAfter(this.alert.dismissAfter);
    }
  }

  private  dismissAlertAfter(millis: number) {
    timer(millis).pipe(
      take(1)
    ).subscribe(_ => this.closeAlert());
  }

  public closeAlert() {
    this.alert = undefined;
  }
}
