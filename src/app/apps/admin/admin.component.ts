import { Component, OnInit, inject } from '@angular/core';
import { StoricoService } from './services/storico.service';
import { Film } from '../film/models/film';
import { AsyncPipe } from '@angular/common';
import { FiltroComponent } from './components/filtro/filtro.component';
import { LoadingComponent } from '../../shared/components/loading/loading.component';

@Component({
  selector: 'app-admin',
  standalone: true,
  imports: [AsyncPipe,FiltroComponent,LoadingComponent],
  templateUrl: './admin.component.html',
  styleUrl: './admin.component.css'
})
export class AdminComponent implements OnInit {


  storicoService=inject(StoricoService);

  films:Film[] = [];
  filteredList:Film[] = [];
  isLoading:boolean=false;

  // copy the list into other variable to use for filetered list
  // and pass to the filter component always the full list
  ngOnInit(): void {
    this.isLoading = true;
    this.storicoService.getStoricoFilm().subscribe({
     next: films => {
      this.films = films
      this.filteredList = films;
    },
    complete: () => this.isLoading=false
    })
  }

  //when user apply the filter we show the filtered list of movies
  updateFilteredMovies(filteredMovies: Film[]) {
    this.filteredList = filteredMovies;
  }


}
