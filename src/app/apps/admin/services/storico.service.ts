import { Injectable, inject } from '@angular/core';
import { Film } from '../../film/models/film';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment.development';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoricoService {

  http=inject(HttpClient);
  baseUrl = environment.apiUrl;
  constructor() { }

  // get all movies
  getStoricoFilm():Observable<Film[]> {
    return this.http.get<Film[]>(`${this.baseUrl}/films`)
  }
}
