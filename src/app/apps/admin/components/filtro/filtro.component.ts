import { Component, EventEmitter, Input, Output, effect, inject, input } from '@angular/core';
import {DateRange, MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {provideNativeDateAdapter} from '@angular/material/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { Film } from '../../../film/models/film';
import {MatButtonModule} from '@angular/material/button';

const today = new Date();
const month = today.getMonth();
const year = today.getFullYear();

@Component({
  selector: 'app-filtro',
  standalone: true,
  providers: [provideNativeDateAdapter()],
  imports: [MatFormFieldModule, MatInputModule, MatDatepickerModule, ReactiveFormsModule,MatButtonModule],
  templateUrl: './filtro.component.html',
  styleUrl: './filtro.component.css'
})
export class FiltroComponent {


  @Input() data:Film[] = [];
  @Output() listaFiltrataEvent = new EventEmitter<Film[]>();
  fb=inject(FormBuilder);
  formFilter: FormGroup;

  listaFiltrata: Film[] = [];

  constructor(){
    this.formFilter = this.fb.group({
      daterange: new FormGroup({
        start:new FormControl(''),
        end:new FormControl('')
      })
    });

  }

  // when apply the filter the list of movies will be filtered by the range of dates
  onSubmit(){
    if(this.formFilter.value.daterange.start == null && this.formFilter.value.daterange.end == null) {
      this.listaFiltrataEvent.emit(this.data);
    } else {
      this.listaFiltrata = this.data.filter(oggetto =>
        (this.formFilter.value.daterange.start >= new Date(oggetto.dataInizio).setHours(0,0,0,0) && this.formFilter.value.daterange.start <= new Date(oggetto.dataFine).setHours(0,0,0,0)) ||
        (this.formFilter.value.daterange.end <= new Date(oggetto.dataFine).setHours(0,0,0,0) && this.formFilter.value.daterange.end >= new Date(oggetto.dataInizio).setHours(0,0,0,0)) ||
        (this.formFilter.value.daterange.start < new Date(oggetto.dataInizio).setHours(0,0,0,0)  && this.formFilter.value.daterange.end >= new Date(oggetto.dataInizio).setHours(0,0,0,0)) ||
        (this.formFilter.value.daterange.end > new Date(oggetto.dataFine).setHours(0,0,0,0) && this.formFilter.value.daterange.inizio <= new Date(oggetto.dataFine).setHours(0,0,0,0))
      );
      this.listaFiltrataEvent.emit(this.listaFiltrata);
    }

  }
}
