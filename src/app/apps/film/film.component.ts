import { Component, OnInit, inject } from '@angular/core';
import { FilmService } from './services/film.service';
import { FilmListComponent } from './components/film-list/film-list.component';
import { Observable } from 'rxjs';
import { Film } from './models/film';
import { AsyncPipe } from '@angular/common';
import { CarouselComponent } from './components/carousel/carousel.component';
import { LoadingComponent } from '../../shared/components/loading/loading.component';

@Component({
  selector: 'app-film',
  standalone: true,
  imports: [FilmListComponent,AsyncPipe,CarouselComponent,LoadingComponent],
  templateUrl: './film.component.html',
  styleUrl: './film.component.css'
})
export class FilmComponent {

  filmService=inject(FilmService);

// Take list of film filtered by today and retrive the list of movies on theatre
  films$: Observable<Film[]> = this.filmService.getFilmsProgrammazione();




}
