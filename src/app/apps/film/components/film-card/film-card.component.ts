import { Component, input } from '@angular/core';
import { RouterLink } from '@angular/router';
import { Film } from '../../models/film';


@Component({
  selector: 'app-film-card',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './film-card.component.html',
  styleUrl: './film-card.component.css'
})
export class FilmCardComponent {

//take film to show in the card
  film=input<Film>();



}
