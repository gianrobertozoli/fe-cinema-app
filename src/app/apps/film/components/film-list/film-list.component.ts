import { Component, effect, input } from '@angular/core';
import { FilmCardComponent } from '../film-card/film-card.component';
import { Film } from '../../models/film';

@Component({
  selector: 'app-film-list',
  standalone: true,
  imports: [FilmCardComponent],
  templateUrl: './film-list.component.html',
  styleUrl: './film-list.component.css'
})
export class FilmListComponent {

  films = input.required<Film[]>();

  constructor(){
  }

}
