import { Component, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FilmService } from '../../services/film.service';
import { Film } from '../../models/film';
import { Observable } from 'rxjs';
import { AsyncPipe } from '@angular/common';

@Component({
  selector: 'app-film-details',
  standalone: true,
  imports: [AsyncPipe],
  templateUrl: './film-details.component.html',
  styleUrl: './film-details.component.css'
})
export class FilmDetailsComponent {

  route = inject(ActivatedRoute);
  filmService = inject(FilmService);

  id:string;
  film$:Observable<Film>;

  constructor(){
    this.id = this.route.snapshot.paramMap.get('id')!;
    this.film$ = this.getFilmById(this.id);
  }

// take film by id to show in the page
  getFilmById(filmId:string):Observable<Film> {
    return this.filmService.getFilmsbyId(filmId)
  }

}
