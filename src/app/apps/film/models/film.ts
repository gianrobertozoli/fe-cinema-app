export interface Film {
  id: number
  titolo: string
  regista: string
  durata: number
  immagine: string
  genere: string
  descrizione: string
  dataInizio: string
  dataFine: string
  numeroSala: number
}
