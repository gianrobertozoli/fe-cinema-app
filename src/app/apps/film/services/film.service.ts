import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment.development';
import { Film } from '../models/film';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  http=inject(HttpClient)

  baseUrl = environment.apiUrl;

  constructor() { }

  // get movies by today date
  getFilmsProgrammazione():Observable<Film[]> {
    return this.http.get<Film[]>(`${this.baseUrl}/films/programmazione`);
  }
  // get movies by id
  getFilmsbyId(filmId:string):Observable<Film> {
    return this.http.get<Film>(`${this.baseUrl}/films/${filmId}`);
  }
}
