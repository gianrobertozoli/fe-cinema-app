export class Alert {
  constructor(
      public msg: string,
      public alertType: Color,
      public dismissAfter?: number,
      public noRemoveOnRoute?: boolean
  ) { }
}

export type Color = 'success' | 'danger' | 'warning' | 'info';
