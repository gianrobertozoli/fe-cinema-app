export interface User {
  id: number
  email: string
  password: string
  fullName: string
}

export interface UserLogin {
  username: string;
  password: string;
}
