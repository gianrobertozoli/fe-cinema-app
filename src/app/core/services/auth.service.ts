import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { environment } from '../../../environments/environment.development';
import { Login } from '../models/login';
import { User } from '../models/user';
import { jwtDecode } from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly JWT_TOKEN = 'JWT_TOKEN';
  private loggedUser: string = '';
  private isAuthenticated: boolean = false;
  private isAuthenticatedSubject = new BehaviorSubject<boolean>(this.isLoggedIn());
  public isAuthenticated$ = this.isAuthenticatedSubject.asObservable();
  private route = inject(Router);

  private http = inject(HttpClient);
  baseUrl = environment.apiUrl;
  constructor() { }

  // login user by email and password
  login(user:{email: string, password: string}): Observable<Login> {
    return this.http.post<Login>(`${this.baseUrl}/auth/signin`, user).pipe(
      tap((tokens) => this.doLoginUser(user.email, JSON.stringify(tokens))
    ))
  }

  // set authenticated user and save the token
  private doLoginUser(email: string, token:string){
    this.loggedUser = email;
    this.storeJwtToken(token);
    this.isAuthenticatedSubject.next(true);
  }

  // save the token on localstorage
  private storeJwtToken(jwt: string) {
    localStorage.setItem(this.JWT_TOKEN,jwt);
  }

  // logout user and remove the token and set user not authenticated
  logOut(){
    localStorage.removeItem(this.JWT_TOKEN);
    this.isAuthenticatedSubject.next(false);
    this.route.navigate(['/film']);
  }

  // check if the token is present
  isLoggedIn() {
    return !!localStorage.getItem(this.JWT_TOKEN);
  }


}
