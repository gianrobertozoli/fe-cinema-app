import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Alert, Color } from '../models/alert';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private readonly subject = new Subject<Alert>();
  public alerts$ = this.subject.asObservable();

  constructor() { }

  // you can send alert to the top of the page
  send(msg: string, alertType: Color = 'danger', dismissAfter?: number, noRemoveOnRoute?:any) {
    this.nextAlert(new Alert(msg, alertType, dismissAfter, noRemoveOnRoute));
    this.gotoTop();
  }

  // set observable
  nextAlert(alert: Alert) {
    this.subject.next(alert);
  }
  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }
}

