import { HttpInterceptorFn } from '@angular/common/http';
import { Login } from '../models/login';

// catch the token from header or localstorage
export const authInterceptor: HttpInterceptorFn = (req, next) => {
  const jwtToken = getJwtToken();
  if(jwtToken) {
 var clone = req.clone({
    setHeaders: {
      Authorization: 'Bearer ' + jwtToken
    }
  })
  return next(clone);
  }
  return next(req);
};

// get from local storage
function getJwtToken():Login | null {
  let tokens:string | null = localStorage.getItem('JWT_TOKEN');
  if(!tokens) return null;
  const token = JSON.parse(tokens).jwt;
  return token;
}
