import { HttpEvent, HttpHandlerFn, HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { AlertService } from '../services/alert.service';


// catch the HTTP error here You can configure every case
export const errorInterceptor: HttpInterceptorFn = (req, next:HttpHandlerFn): Observable<HttpEvent<unknown>> => {
  const alertService = inject(AlertService);
  return next(req).pipe(
    catchError((err) => {
      console.log('Caught in catchError', err.message);
      if (err) {
        switch (err.status) {
          case 400:
            console.error('unauthorized', err.status.toString() );
            alertService.send(`unauthorized ${err.status.toString()}`, 'warning');
            break;
          case 401:
            console.error('unauthorized', err.status.toString());
            alertService.send(`unauthorized ${err.status.toString()}`, 'warning');
            break;
          default:
            console.error('error', err.status.toString());
            alertService.send(`error ${err.status.toString()}`, 'warning');
            break;
        }
    }
    return throwError(()=> err);
  }))
}
