import { Component } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import{faYoutube,faFacebook,faLinkedin,faInstagram} from '@fortawesome/free-brands-svg-icons'

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [FaIconComponent],
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.css'
})
export class FooterComponent {

  faYoutube = faYoutube;
  facebook = faFacebook;
  linkedin = faLinkedin;
  instagram = faInstagram;

}
