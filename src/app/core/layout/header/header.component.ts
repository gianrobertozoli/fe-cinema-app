import { Component, OnChanges, OnInit, SimpleChanges, inject, input } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import{faUser} from '@fortawesome/free-solid-svg-icons'
import { LoginComponent } from '../login/login.component';
import { Router, RouterLink } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [FaIconComponent,LoginComponent,RouterLink],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent {

//take input to check if user is logged in
  isAuthentecated=input<boolean>()

  user = faUser;

  router = inject(Router)
  authService = inject(AuthService)

  //navigate to login page
  btnClick():void {
    this.router.navigateByUrl('/login');
  }

  //logout and remove the token also to navigate to homepage
  logout() {
    this.authService.logOut();
  }

}
