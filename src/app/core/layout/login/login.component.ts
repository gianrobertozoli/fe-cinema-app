import { Component, inject } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  fb = inject(FormBuilder)
  authenticationService = inject(AuthService)
  router = inject(Router)

  loginForm = this.fb.group({
    username: ['',[Validators.required,Validators.email]],
    password: ['',[Validators.required]],
  });

  submit(){
     this.authenticationService.login({
      email:this.loginForm.value.username ?? '',
      password:this.loginForm.value.password ?? ''
    }
    ).subscribe(res => {
      this.router.navigate(['/admin'])
    }
    )

  }
}
