import { Component, inject } from '@angular/core';
import { HeaderComponent } from './core/layout/header/header.component';
import { FooterComponent } from './core/layout/footer/footer.component';
import { CarouselComponent } from './apps/film/components/carousel/carousel.component';
import { FilmComponent } from './apps/film/film.component';
import { RouterOutlet } from '@angular/router';
import { AlertComponent } from './shared/components/alert/alert.component';
import { AlertContainerComponent } from './shared/components/alert-container/alert-container.component';
import { Alert } from './core/models/alert';
import { AlertService } from './core/services/alert.service';
import { AuthService } from './core/services/auth.service';


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [HeaderComponent,FooterComponent,CarouselComponent,RouterOutlet,AlertContainerComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  alert: Alert | undefined = undefined;
  isAuthenticated:boolean = false;
  alertService = inject(AlertService);
  authService = inject(AuthService);

ngOnInit(): void {
  //Subscription to take all the alert message at the top of the page
  this.alertService.alerts$.subscribe(alert => {
    this.alert = alert;
  });
  //Subscription to take if the user is logged in and there is the token
  this.authService.isAuthenticated$.subscribe(auth => {
    this.isAuthenticated = auth})
}
}
